
package ioc.xtec.cat.calculfiguresgeometriques;

import java.util.Scanner;

/**
 *
 * @author estherpm
 */
public class Rectangle implements FiguraGeometrica {
    private final double longitud;
    private final double ample;
    
    public Rectangle () {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introduïu la longitud del rectangle: ");
        this.longitud = scanner.nextDouble();
        System.out.println("Introduïu l'ample del rectangle: ");
        this.ample = scanner.nextDouble();
    }
    
    @Override
    public double calcularArea() {
        return longitud * ample;
    }
    
    @Override
    public double calcularPerimetre() {
        return 2*longitud + 2*ample;
    }
}