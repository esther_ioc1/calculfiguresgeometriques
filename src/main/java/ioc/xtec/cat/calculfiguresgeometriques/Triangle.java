
package ioc.xtec.cat.calculfiguresgeometriques;

import java.util.Scanner;

/**
 *
 * @author estherpm
 */
public class Triangle implements FiguraGeometrica{
    private final double base;
    private final double costat1;
    private final double costat2;
    
    public Triangle() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introduïu la base del triangle: ");
        this.base = scanner.nextDouble();
        System.out.println("Introduïu el costat 1 del triangle: ");
        this.costat1 = scanner.nextDouble();
        System.out.println("Introduïu el costat 2 del triangle: ");
        this.costat2 = scanner.nextDouble();
    }
    
    @Override
    public double calcularArea() {
        double semiPer = calcularPerimetre()/2;
        double area = Math.sqrt(semiPer * (semiPer - base) * (semiPer - costat1) * (semiPer - costat2));
        return area;
    }
    
    @Override
    public double calcularPerimetre() {
        return base + costat1 + costat2;
    }
}